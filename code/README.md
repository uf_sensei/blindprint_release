GSHADE
======
RUNNING
-------
type the following :
"make"
in first terminal window type :
"./dst.exe 1 client.txt server.txt"
in second terminal window type :
"./dst.exe 0 client.txt server.txt"

--- Description ---

Privacy preserving distance computation framework for: Hamming distance, Euclidean distance, Scalar Product and Normalized Hamming distance (currently not implemented) on the publication [1]. The GSHADE code is licensed under AGPLv3, see the LICENSE file for a copy of the license. 
--- Requirements ---
- Miracl (set up in /util/Miracl/ )
- The GMP library
- OpenSSL

--- Compiling ---

- Compile the Miracl library for elliptic curve cryptography (util/Miracl) using "bash linux" / "bash linux64" (depending on the system)
- Compile the distance computation framework by invoking "make" in the folder of the README file

--- Execution ---

Currently, the server routine can be invoked by starting "./dst.exe 0" in one terminal and the connecting client can be invoked using "./dst.exe 1" in a second terminal on the same machine. The parameters can be changed in the code. 
The second parameter is the client input file and third parameter is server input file.

--- Current Status ---
Currently, the Euclidean distance and the Scalar product have been tested and should work properly. The Hamming distance is executed but might result in the wrong result. The Normalized Hamming distance is currently not implemented, due to problems with the internal program structure. 


[1] [Julien Bringer, Herve Chabanne, Melanie Favre, Alain Patey, Thomas Schneider, and Michael Zohner. GSHADE: Faster Privacy-Preserving Distance Computation and Biometric Identification, 2nd ACM Workshop on Information Hiding and Multimedia Security (IH&MMSEC 2014)](http://thomaschneider.de/papers/BCFPSZ14.pdf)

--- Input ---
The server.txt and client.txt contain input to server and client respectively
First value in each gives number of entries and second value gives size of each entry.
For sever.txt 2 lines are required for each entry and for client.txt 3 lines are required for each entry(see protocol).


